# Discourse Segmentation and Morphosyntactic Analysis

## Table of contents

- [Set-up](#set-up)
    - [Virtual Environment and Requirements](#virtual-environment-and-requirements)
    - [SpaCy Models](#spacy-models)
- [Discourse Segmentation for DISRPT 2021](#discourse-segmentation-for-disrpt-2021)
    - [Data](#data)
    - [Preprocessing](#preprocessing)
    - [Training](#training)
    - [Prediction](#prediction)
    - [Evaluation](#evaluation)
- [Tense, Mood, Voice and Modality Tagging for 11 Languages](#tense-mood-voice-and-modality-tagging-for-11-languages)
    - [Quick Example](#quick-example)
    - [Language Resources](#language-resources)
- [License/Citation](#licensecitation)

## Set-Up

### Virtual Environment and Requirements

Clone the repository, set-up a virtual environment and install the requirements:

```sh
git clone https://gitlab.gwdg.de/tillmann.doenicke/disrpt2021-tmvm.git
cd disrpt2021-tmvm
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```

### SpaCy Models

Install pretrained [spaCy](https://spacy.io/) models:

```sh
sh spacy_models.sh
```

This installs the models for the languages of DISRPT 2021. If you want to use spaCy models for other languages; you have to install them separately, or you train them on the UD treebanks as described [here](https://github.com/explosion/projects/tree/v3/pipelines/tagger_parser_ud) and [here](https://spacy.io/usage/projects).

We trained new models on the [UD treebanks](https://universaldependencies.org/) v2.8 for German (on HDT), Basque (on BDT), Persian (on PerDT) and Turkish (on Kenet); they are saved in `resources/tagger_parser_ud/packages`. Note that these models do not include a lemmatizer. We created lemma dictionaries for the custom models with `resources/lemmatization/create_dicts.py`; they are saved in `resources/lemmatization`. See `enrich_tok_files.py` for how to use them.

## Discourse Segmentation for DISRPT 2021

### Data

Download the `data` and `utils` folder from [https://github.com/disrpt/sharedtask2021](https://github.com/disrpt/sharedtask2021).

If you have an LDC license, reconstruct the tokens in the delexicalised datasets using `utils/process_underscores.py`.

### Preprocessing

For preprocessing the `.tok` files, run:

```sh
python enrich_tok_files.py 0
```

This creates a file ending with `.tok.spacy` for every `.tok` file.

The parameter indicates whether the datasets `eng.pdtb.pdtb`, `eng.rst.gum`, `eng.rst.rstdt`, `tur.pdtb.tdb` and `zho.pdtb.cdtb` should be excluded (`0`) or included (`1`) in preprocessing. If you have no LDC license and these files are still delexicalised, you should use `0`. If you reconstructed the tokens for these files, you should use `1`.

### Training

For training a new classifier, run:

```sh
python train.py path-to-train-file
```

where `path-to-train-file` could be e.g. `data/deu.rst.pcc/deu.rst.pcc_train.conllu` or `data/deu.rst.pcc/deu.rst.pcc_train.tok.spacy` (don't use the `.tok` files). The corresponding development set will be used for hyperparameter tuning. The trained model is saved to `models/[train-file].pickle`, e.g. `models/deu.rst.pcc_train.conllu.pickle`.

### Prediction

For creating predictions with a trained classifier, run:

```sh
python predict.py path-to-model path-to-test-file path-to-pred-file
```

where `path-to-model` is the path to the trained model and `path-to-test-file` could be e.g. `data/deu.rst.pcc/deu.rst.pcc_test.conllu` or `data/deu.rst.pcc/deu.rst.pcc_test.tok.spacy` (don't use the `.tok` files). `path-to-pred-file` is an optional parameter that specifies the output file; if it is ommited, the predictions will be saved to a file in `predictions` that corresponds to `path-to-test-file`, e.g. `predictions/deu.rst.pcc/deu.rst.pcc_test.conllu`.

### Evaluation

For evaluating the predictions, run:

```sh
python utils/seg_eval.py path-to-test-file path-to-pred-file
```

where `path-to-test-file` should be a file in `data` and `path-to-pred-file` should be the corresponding file in `predictions`.

### Paper Results

To reproduce the results of the system paper, run:

```sh
python main_results.py 0
```

The parameter, again, indicates whether the datasets `eng.pdtb.pdtb`, `eng.rst.gum`, `eng.rst.rstdt`, `tur.pdtb.tdb` and `zho.pdtb.cdtb` should be excluded (`0`) or included (`1`) in the plain condition.

## Tense, Mood, Voice and Modality Tagging for 11 Languages

If you are just interested in TMVM tagging (independently of discourse segmentation), you may read this section.

### Quick Example

(You can also find the code in `tmvm_example.py`.)

First, you need some imports:

```python
import spacy

from extract_features import compute_grammatical_feats, get_clause_components
from split_units_ud import split_into_clauses, unit_from_sentence
```

We want to compute TMVM for an English example text:

```python
# input text and language
text = "OK, I will try to send an email."
language = "eng"
```

Before you can do TMVM tagging, you have to split your text into sentences and perform a morphological analysis for every token. A tool which can do this for you is spaCy:

```python
# process text with English spaCy model
nlp = spacy.load("en_core_web_lg")
doc = nlp(text)
```

Next, you have to convert spaCy's token objects into the dictionary format that is needed for the TMVM tagger:

```python
for sent in doc.sents:
    # iterate over sentences
    
    # convert spaCy tokens into CoNLL dict
    sent_tokens = []
    for i, token in enumerate(sent):
        sent_tokens.append({
            "id" : str(i+1), 
            "form" : token.text, 
            "lemma" : token.lemma_, 
            "upos" : token.pos_, 
            "tag" : token.tag_, 
            "feats" : {k : v for k, v in [tuple(m.split("=")) for m in str(token.morph).split("|") if "=" in m]}, 
            "head" : str(list(sent).index(token.head)+1), 
            "deprel" : token.dep_
        })
```

From the tokens, you create a sentence object that you can split into clauses:

```python
    # create sentence unit object
    sentence = unit_from_sentence(sent_tokens, language)
    
    # split sentence into clauses
    clauses = split_into_clauses(sentence)
```

Finally, you can split each clause into its components, including the composite verb from which you can compute the grammatical features.

```python
    for clause in clauses:
        # iterate over clauses
        print(clause)
        
        # split clause into composite verb, NPs and other elements
        composite_verb, NPs, other = get_clause_components(clause)
        print([w["form"] for w in composite_verb])

        # extract grammatical features of the verb
        verb_feats = compute_grammatical_feats(composite_verb, language)
        print(verb_feats)
        
        print()
```

Our example text gives the following output (three clauses):

```
OK
[]
{}

, I will try .
['will', 'try']
{'Aspect': {'Imp'}, 'Mood': {'Ind'}, 'Tense': {'Fut'}, 'VerbForm': {'Fin'}, 'Voice': {'Act'}}

to send an email
['to', 'send']
{'VerbForm': {'Inf'}}
```

### Language Resources

Currently, TMVM tagging works for German (`deu`), English (`eng`), Basque (`eus`), Persian (`fas`), French (`fra`), Dutch (`nld`), Portuguese (`por`), Russian (`rus`), Spanish (`spa`), Turkish (`tur`) and Chinese (`zho`).

If you want to make it work for a new language, you have to add two resources in `resources/inflection`:

1) `lang_data.py`: Auxiliary verbs, modal verbs and basic word order of the language.
2) `table.csv`: All composite verb forms (simple and compound verb forms) of the language.

    Let's illstrate this with *will* + [infinitive] as in *will write*:
    <table>
        <tr>
            <th style="border-left: 1px solid;"></th><th style="border-left: 1px solid;"></th><th style="border-left: 1px solid;" colspan="5">grammatical features</th><th style="border-left: 1px solid;" colspan="5">morphological features of main verb</th><th style="border-left: 1px solid;" colspan="7">lemma and morphological features of auxiliary verbs</th>
        </tr>
        <tr>
            <th style="border-left: 1px solid;">example</th><th style="border-left: 1px solid;">lang</th><th style="border-left: 1px solid;">VerbForm</th><th>Tense</th><th>Aspect</th><th>Mood</th><th>Voice</th><th style="border-left: 1px solid;">VerbForm</th><th>Tense</th><th>Aspect</th><th>Mood</th><th>Voice</th><th style="border-left: 1px solid;">lemma</th><th>VerbForm</th><th>Tense</th><th>Aspect</th><th>Mood</th><th>Voice</th><th style="border-left: 1px solid;">...</th>
        </tr>
        <tr>
            <td style="border-left: 1px solid;">will write</td><td style="border-left: 1px solid;">eng</td><td style="border-left: 1px solid;">Fin</td><td>Fut</td><td>Imp</td><td>Ind</td><td>Act</td><td style="border-left: 1px solid;">Inf</td><td>[Inf]</td><td>[Inf]</td><td>[Inf]</td><td>[Inf]</td><td style="border-left: 1px solid;">AUX3</td><td>Fin</td><td>Pres</td><td>[Fin]</td><td>Ind</td><td>[Fin]</td><td style="border-left: 1px solid;"></td>
        </tr>
    </table>

    The first column is only for humans. Since one table is used for all languages, the second column contains the language code `eng`. The next 5 columns contain the grammatical features (VerbForm, Tense, Aspect, Mood, Voice) that should be assigned to *will write*. Then the lemmas and morphological features of the individual verbs follow, from the syntactically lowest verb (the main verb) to the syntactically highest verb (the finite verb). So, for VO languages like English the speaking order is reversed: *write will*. For the main verb, there is no lemma column; for the auxiliary verbs, the lemma key from `lang_data.py` should be used, e.g. `AUX3` for *will*. Features that are not morphologically marked should receive the value `[VerbForm]`, e.g. `[Inf]` or `[Fin]`.

## License/Citation

This work is licensed under a Creative Commons Attribution 4.0 International License.

If you use this code, you should cite the following paper in your work:

> Tillmann Dönicke (2021). "Delexicalised Multilingual Discourse Segmentation for DISRPT 2021 and Tense, Mood, Voice and Modality Tagging for 11 Languages". In Proceedings of Discourse Relation Parsing and Treebanking (DISRPT2021).
