import spacy

from extract_features import compute_grammatical_feats, get_clause_components
from split_units_ud import split_into_clauses, unit_from_sentence

# input text and language
text = "OK, I will try to send an email."
language = "eng"

# process text with English spaCy model
nlp = spacy.load("en_core_web_lg")
doc = nlp(text)

for sent in doc.sents:
    # iterate over sentences
    
    # convert spaCy tokens into CoNLL dict
    sent_tokens = []
    for i, token in enumerate(sent):
        sent_tokens.append({
            "id" : str(i+1), 
            "form" : token.text, 
            "lemma" : token.lemma_, 
            "upos" : token.pos_, 
            "tag" : token.tag_, 
            "feats" : {k : v for k, v in [tuple(m.split("=")) for m in str(token.morph).split("|") if "=" in m]}, 
            "head" : str(list(sent).index(token.head)+1), 
            "deprel" : token.dep_
        })
    
    # create sentence unit object
    sentence = unit_from_sentence(sent_tokens, language)
    
    # split sentence into clauses
    clauses = split_into_clauses(sentence)
    
    for clause in clauses:
        # iterate over clauses
        print(clause)
        
        # split clause into composite verb, NPs and other elements
        composite_verb, NPs, other = get_clause_components(clause)
        print([w["form"] for w in composite_verb])

        # extract grammatical features of the verb
        verb_feats = compute_grammatical_feats(composite_verb, language)
        print(verb_feats)
        
        print()