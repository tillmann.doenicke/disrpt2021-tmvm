import json
import os
import spacy
import sys
from spacy.language import Language

from file_reader import read_file


def lemmatizer_(lang, doc):
    """Dictionary lemmatizer.
        Uses the resources in `resources/lemmatization`.

    Args:
        lang (str): Language (two-letter code).
        doc (`Doc`): SpaCy document.
    
    Returns:
        `Doc`: Document where `token.lemma_` is set.
    
    """
    with open(os.path.join("resources", "lemmatization", lang + ".json"), "r") as f:
        form_upos_lemma, form_lemma = json.load(f)
        
        for token in doc:
            try:
                token.lemma_ = form_upos_lemma[token.text][token.pos_]
            except KeyError:
                try:
                    token.lemma_ = form_lemma[token.text]
                except KeyError:
                    token.lemma_ = token.text
        return doc


@Language.component("lemmatizer_deu")
def lemmatizer_de(doc):
    return lemmatizer_("de", doc)

@Language.component("lemmatizer_fas")
def lemmatizer_fa(doc):
    return lemmatizer_("fa", doc)

@Language.component("lemmatizer_eus")
def lemmatizer_eu(doc):
    return lemmatizer_("eu", doc)

@Language.component("lemmatizer_tur")
def lemmatizer_tr(doc):
    return lemmatizer_("tr", doc)


if __name__ == "__main__":

    args = sys.argv
    if len(args) > 1:
        underscores_processed = bool(int(args[1]))
        if underscores_processed:
            corpora_with_underscores = []
        else:
            # corpora that don't have tokens (will be excluded in this script)
            corpora_with_underscores = ["eng.pdtb.pdtb", "eng.rst.gum", "eng.rst.rstdt", "tur.pdtb.tdb", "zho.pdtb.cdtb"]
    else:
        raise Exception("You must specify whether the datasets with underscores are reconstructed (1) or not (0).")
    
    # path to the spacy models for German, Basque and Turkish
    path_to_own_spacy_models = os.path.join("resources", "tagger_parser_ud", "packages")
    
    # spacy models for all languages
    spacy_models = {
        "deu" : spacy.load(os.path.join(path_to_own_spacy_models, "de_ud_de_hdt-0.0.0", "de_ud_de_hdt", "de_ud_de_hdt-0.0.0")),
        "eng" : spacy.load("en_core_web_lg"),
        "eus" : spacy.load(os.path.join(path_to_own_spacy_models, "eu_ud_eu_bdt-0.0.0", "eu_ud_eu_bdt", "eu_ud_eu_bdt-0.0.0")),
        "fas" : spacy.load(os.path.join(path_to_own_spacy_models, "fa_ud_fa_perdt-0.0.0", "fa_ud_fa_perdt", "fa_ud_fa_perdt-0.0.0")),
        "fra" : spacy.load("fr_core_news_lg"),
        "nld" : spacy.load("nl_core_news_lg"),
        "por" : spacy.load("pt_core_news_lg"),
        "rus" : spacy.load("ru_core_news_lg"),
        "spa" : spacy.load("es_core_news_lg"),
        "tur" : spacy.load(os.path.join(path_to_own_spacy_models, "tr_ud_tr_kenet-0.0.0", "tr_ud_tr_kenet", "tr_ud_tr_kenet-0.0.0")),
        "zho" : spacy.load("zh_core_web_lg")
    }

    # add lemmatizers to custom models
    for lang in ["deu", "fas", "eus", "tur"]:
        spacy_models[lang].add_pipe("lemmatizer_" + lang)
    
    # path to the corpora
    path_to_data = "data"
    corpora = sorted(os.listdir(path_to_data))
    
    # iterate over all corpora
    for corpus in corpora:
        
        if corpus in corpora_with_underscores:
            # exclude corpora without tokens
            continue
        
        path_to_corpus = os.path.join(path_to_data, corpus)
        if os.path.isdir(path_to_corpus):

            # select the correct spacy model
            lang = corpus.split(".")[0]
            spacy_model = spacy_models[lang]

            # iterate over all files in the current corpus
            files = sorted(os.listdir(path_to_corpus))
            for file in files:
                
                if file.endswith(".tok"):
                    # read the documents from the `.tok` file
                    path_to_file = os.path.join(path_to_corpus, file)
                    documents = read_file(path_to_file)
                    
                    output_lines = []
                    for document in documents:
                        for sentence in document:

                            # line as in the `.tok` file
                            conll_tok = sentence.serialize().split("\n")
                            
                            # get the header of the current sentence
                            header = []
                            for i, line in enumerate(conll_tok):
                                if line.startswith("#"):
                                    header.append(line)
                                else:
                                    conll_tok = conll_tok[i:]
                                    break
                            output_lines.extend(header)
                            
                            # process the sentence with spacy
                            doc = spacy.tokens.doc.Doc(spacy_model.vocab, words=[token["form"] for token in sentence])
                            for _, component in spacy_model.pipeline:
                                doc = component(doc)
                            doc_tokens = list(doc)
                            
                            # format the new output lines
                            while len(doc_tokens) > 0:
                                token_tok = conll_tok.pop(0).split("\t")
                                token_doc = doc_tokens.pop(0)
                                sent = list(token_doc.sent)
                                conll_line = [str(sent.index(token_doc)+1), token_tok[1], token_doc.lemma_, token_doc.pos_, token_doc.tag_, (str(token_doc.morph) if len(str(token_doc.morph)) > 0 else "_"), str(0 if token_doc == token_doc.head else (sent.index(token_doc.head)+1)), token_doc.dep_, "_", token_tok[-1]]
                                conll_line = "\t".join(conll_line)
                                output_lines.append(conll_line)
                                if len(doc_tokens) == 0 or doc_tokens[0].is_sent_start:
                                    output_lines.append("")
                    
                    # write new file with `.tok.spacy` ending
                    print(path_to_file)
                    with open(path_to_file + ".spacy", "w") as f2:
                        f2.write("\n".join(output_lines))