import itertools
import json
import re
import string
import unicodedata

from resources.inflection.lang_data import LANG_DATA, LANG_TABLE
from split_units_ud import *


UNIVERSAL_DEPS = set(["acl", "advcl", "advmod", "amod", "appos", "aux", "case", "cc", "ccomp", "clf", "compound", "conj", "cop", "csubj", "dep", "det", "discourse", "dislocated", "expl", "fixed", "flat", "goeswith", "iobj", "list", "mark", "nmod", "nsubj", "nummod", "obj", "obl", "orphan", "parataxis", "punct", "reparandum", "root", "vocative", "xcomp"])

UNIVERSAL_TAGS = set(["ADJ", "ADP", "ADV", "AUX", "CCONJ", "DET", "INTJ", "NOUN", "NUM", "PART", "PRON", "PROPN", "PUNCT", "SCONJ", "SPACE", "SYM", "VERB", "X"])

LEXICAL_FEATS = set(["PronType", "NumType", "Poss", "Reflex", "Foreign", "Abbr", "Typo"])

NOMINAL_FEATS = set(["Case", "Person", "Number", "Gender", "Degree", "Definite", "Animacy"])

VERBAL_FEATS = set(["VerbForm", "Tense", "Aspect", "Mood", "Voice"])#, "Evident", "Polite", "Clusivity", "Polarity"])


def get_clause_components(clause):
    """Split a clause into components.
    
    Args:
        clause (`Unit`): A clause.
    
    Returns:
        list of `Token`: The compound verb form.
        list of `Unit`: All NPs.
        list of `Token`: All clause-level dependents, e.g. connectives and adverbs.

    """
    # get all NPs
    NPs = split_into_NPs(clause)
    
    # select the tokens outside NPs
    NP_tokens = sum([NP.tokens for NP in NPs], [])
    free_tokens = [token for token in clause.tokens if token not in NP_tokens]
    
    # get the (compound) verb, including particles and compounds
    compound_verb = [token for token in free_tokens if (unify_tags(token["upos"]) in ["VERB", "AUX"]) or ((unify_tags(token["upos"]) == "PART" or unify_deps(deprel(token)) == "compound") and get_head(clause, token)["upos"] in ["VERB", "AUX"])]
    compound_verb = sorted(compound_verb, key=lambda token: clause.tokens.index(token))
    
    # select the still-free tokens that are directly subordinate to the root
    free_tokens = [token for token in free_tokens if token not in compound_verb]
    free_tokens = [token for token in free_tokens if get_head(clause, token) == clause.root]
    free_tokens = sorted(free_tokens, key=lambda token: clause.tokens.index(token))
    
    return compound_verb, NPs, free_tokens


def extract_feats_connective(tokens):
    """Extract features for a connective.

    Args:
        tokens (list of (dict of str:obj)): List of first tokens of a clause.
    
    Returns:
        dict of str:obj: The feature dict.
    
    """
    feats = {}
    for i, token in enumerate(tokens):
        prefix = str(i+1)
        feats[prefix + "_upos"] = unify_tags(token["upos"])
        feats[prefix + "_deprel"] = unify_deps(deprel(token))
        feats[prefix + "_head"] = str(token["head"] if token["head"] <= 5 else 0)
    feats["max"] = len(tokens)
    return feats


def extract_feats_with_context(clauses, index, grammatical_features):
    """Extract all features for a clause and its two neighbours.
    
    Args:
        clauses (list of `Unit`): All clauses in a document.
        index (int): Index of the current clause.
        grammatical_features (boolean): If True, grammatical features are computed for the compound verb;
            if False, morphological features are used instead.
    
    Returns:
        dict of str:boolean: The feature dict.

    """
    feats = {}
    for offset in [-1, 0, 1]:
        i = index + offset
        if -1 < i and i < len(clauses):
            clause_feats = extract_feats(clauses[i], grammatical_features)
            if offset != 0:
                if clauses[i].parent_unit == clauses[index].parent_unit:
                    clause_feats["same_sent"] = True
                    if get_head(clauses[i].parent_unit, clauses[i].root) in clauses[index].tokens:
                        clause_feats["subordinate"] = True
                    if get_head(clauses[index].parent_unit, clauses[index].root) in clauses[i].tokens:
                        clause_feats["superordinate"] = True
            feats = aggregate_feats(feats, clause_feats, str(offset))
    return feats


def extract_feats(clause, grammatical_features):
    """Extract all features for the components of a clause.
        - extracts clause-level features
        - extracts verb-form features
        - extracts NP features
        - extracts discourse features
    
    Args:
        clause (`Unit`): A clause.
        grammatical_features (boolean): If True, grammatical features are computed for the compound verb;
            if False, morphological features are used instead.
    
    Returns:
        dict of str:boolean: The feature dict.

    """
    feats = {}
    clause_feats = extract_clause_feats(clause)
    feats = aggregate_feats(feats, clause_feats, "clause")
    compound_verb, NPs, free_tokens = get_clause_components(clause)
    verb_feats = extract_verb_feats(compound_verb, clause.language, grammatical_features)
    feats = aggregate_feats(feats, verb_feats, "verb")
    for NP in NPs:
        NP_feats = extract_NP_feats(NP)
        feats = aggregate_feats(feats, NP_feats, "NP")
    for token in free_tokens:
        disc_feats = extract_disc_feats(token)
        feats = aggregate_feats(feats, disc_feats, "disc")
    return feats
    

def extract_clause_feats(clause):
    """Extract clause-level features.
    
    Args:
        clause (`Unit`): A clause.
    
    Returns:
        dict of str:boolean: The feature dict.

    """
    feats = {}
    feats[unify_deps(deprel(clause.root))] = True
    feats[unify_tags(clause.root["upos"])] = True
    for token in clause.prec_punct:
        punct = unify_punct(token["lemma"])
        if punct is not None:
            feats["prec=" + punct] = True
    for token in clause.succ_punct:
        punct = unify_punct(token["lemma"])
        if punct is not None:
            feats["succ=" + punct] = True
    return feats


def check_feat(verb, feat_name, feat_val):
    """Check whether a feature-value combination exists in a word's feature dict.

    Args:
        verb (dict of str:obj): Dictionary representing a word, including the morphological features in `verb["feats"]`.
        feat_name (str): Feature name.
        feat_val (str): Feature value.
    
    Returns:
        boolean: True iff the feature-value combination is in `verb["feats"]`.
    
    """
    return feat_name in verb["feats"] and feat_val in verb["feats"][feat_name]


def get_morph_feats(token, language):
    """Prepare a token for grammatical analysis.
        - "id", "form", "head" and "deprel" are kept unmodified
        - "lemma", "upos" and "feats" are converted into a (more) unified format

    Args:
        token (dict of str:obj): Dictionary representing a token, including the morphological features in `token["feats"]`.
        language (str): Language of the token.
    
    Returns:
        dict of str:obj: Formatted token dict.
    
    """
    feats = {name : token[name] for name in ["id", "form", "head", "deprel"]}
    feats["lemma"] = lemma(token, language)
    feats["upos"] = unify_tags(token["upos"])
    feats["feats"] = {}
    if token["feats"] is not None:
        for fname in token["feats"]:
            fval = token["feats"][fname]
            feat_name, feat_vals = unify_morphs(fname, fval)
            if fname in VERBAL_FEATS:
                feats["feats"][feat_name] = set(feat_vals)
    return feats


def compute_grammatical_feats(compound_verb, language, reduce_infinite_forms=True):
    """Return the grammatical analysis of a compound verb form.

    Args:
        compound_verb (list of (dict of str:obj)): The tokens of the compound verb.
        language (str): The language code.
        reduce_infinite_forms (boolean): If True, all features except "VerbForm" are removed from an infinite analysis.
            If False, all features are kept.
            Motivation: It most languages it does not make much sense to assign tense, voice etc. to infinitives and participles;
                since these infinite forms are mostly used to build finite forms.
    
    Returns:
        dict of str:(set of str): The analysis of the compound form.
            Possible keys and values (not always given):
                - "VerbForm": "Fin", "Inf", "Part"
                - "VerbForm_": "Adj", "Adv", "Noun", "Verb"
                - "Tense": "Fut", "Imp", "Past", "Pqp", "Pres"
                - "Aspect": "Hab", "Imp", "Perf", "Prog", "Prosp"
                - "Mood": "Cnd", "Des", "Imp", "Ind", "Nec", "Opt", "Pot", "Sub"
                - "Voice": "Act", "Cau", "Mid", "Pass"
                - "Modality": "OBL", "POS", "VOL"
    
    """
    lang_data = LANG_DATA[language]
    aux_lemmas = set([lemma for cat in lang_data["aux"] for lemma in lang_data["aux"][cat]])
    mod_lemmas = set([lemma for cat in lang_data["mod"] for lemma in lang_data["mod"][cat]])

    compound_verb = [get_morph_feats(token, language) for token in compound_verb]

    # Russian
    # The word "бы" behaves more like a particle than a verb but is tagged as AUX. 
    # It further does not follow the linear syntactic ordering that is required for 
    # this algorithm. Therefore, it is converted to a particle.
    if language == "rus":
        for token in compound_verb:
            if token["lemma"] == u"бы":
                token["upos"] = "PART"
                token["feats"]["VerbForm"] = set()
                token["feats"]["Mood"] = set(["Cnd"])

    # 1. copy the features from particles to their head verbs; remove particles afterwards
    # 2. convert light-verb components to full verbs
    for token in compound_verb:
        if token["upos"] == "PART":
            for i, verb in enumerate(compound_verb):
                if verb["id"] == token["head"]:
                    for fname in token["feats"]:
                        if fname not in verb["feats"]:
                            verb["feats"][fname] = set()
                        verb["feats"][fname].update(token["feats"][fname])
                    compound_verb[i] = verb
                    break
        elif token["deprel"] == "compound:lvc":
            token["upos"] = "VERB"
            token["feats"] = {"VerbForm" : set(["Vnoun"])}
    verbs = [token for token in compound_verb if token["upos"] in ["VERB", "AUX"]]
    
    # bring the verbs in the order of an OV language
    # basically: from syntactically lowest to syntactically highest
    if not lang_data["OV"]:
        verbs = list(reversed(verbs))
    
    # Unfortunately, not all verbs have a verb form.
    # However, only finite verbs can have mood;
    # thus verbs with "Mood" are set to be finite.
    for verb in verbs:
        if "VerbForm" not in verb["feats"]:
            if "Mood" in verb["feats"]:
                verb["feats"]["VerbForm"] = set(["Fin"])

    # select the syntactically highest finite verb
    # remove all other finite verbs
    # counteract finite-verb movement (affects e.g. V2 languages)
    infinite_verbs = [verb for verb in verbs if not check_feat(verb, "VerbForm", "Fin")]
    finite_verbs = [verb for verb in verbs if check_feat(verb, "VerbForm", "Fin")]
    if len(finite_verbs) > 1:
        finite_verbs = [finite_verbs[-1]]
    verbs = infinite_verbs + finite_verbs

    # [conjunct rules; left out in this implementation]
    
    if len(verbs) > 0:

        # separate the verbs into auxiliaries, modals and main verbs
        # select the syntactically highest main verb
        # cut off all lower verbs
        main_verbs = []
        for verb in verbs:
            if verb["lemma"] in aux_lemmas:
                verb["category"] = "aux"
            elif verb["lemma"] in mod_lemmas:
                verb["category"] = "mod"
            else:
                verb["category"] = "main"
                #verb["lemma"] = "#main#"
                main_verbs.append(verb)
        if len(main_verbs) == 0:
            verbs[0]["category"] = "main"
            #verbs[0]["lemma"] = "#main#"
            main_verbs.append(verbs[0])
        verbs = verbs[verbs.index(main_verbs[-1]):]

        for verb in verbs:
            # Cross-lingual unification of present participles:
            # convert gerunds to present participles.
            if check_feat(verb, "VerbForm", "Ger"):
                verb["feats"]["VerbForm"].remove("Ger")
                verb["feats"]["VerbForm"].add("Part")
                if "Tense" not in verb["feats"]:
                    verb["feats"]["Tense"] = set()
                verb["feats"]["Tense"].add("Pres")
            # "Rapid" is not an official UD value for Aspect and not used by all languages:
            # replace it by "Perf" (see http://coltekin.github.io/gk-treebank/feat/Aspect)
            if check_feat(verb, "Aspect", "Rapid"):
                verb["feats"]["Aspect"].remove("Rapid")
                verb["feats"]["Aspect"].add("Perf")
            # "Gen" is not an official UD value for Mood and not used by all languages:
            # replace it by "Ind" (see http://coltekin.github.io/gk-treebank/feat/Mood)
            if check_feat(verb, "Mood", "Gen"):
                verb["feats"]["Mood"].remove("Gen")
                verb["feats"]["Mood"].add("Ind")

        # shift the features of modal verbs to syntactically lower position
        for i in reversed(range(1, len(verbs))):
            if verbs[i]["category"] == "mod":
                verbs[i-1]["feats"] = verbs[i]["feats"]

        while len(verbs) > 0:
            main_lemma = verbs[0]["lemma"]
            verbs[0]["category"] = "main"
            verbs[0]["lemma"] = "#main#"

            # determine all combinations of morphological features
            num_feats = 0
            feature_combinations = []
            for verb in verbs:
                if verb["category"] != "mod":
                    feats = verb["feats"]
                    num_feats += len(feats)
                    feat_names = feats.keys()
                    feat_vals = (feats[feat_name] for feat_name in feat_names)
                    feat_combos = []
                    for feat_combo in itertools.product(*feat_vals):
                        verb_ = {k : verb[k] for k in verb}
                        verb_["feats"] = dict(zip(feat_names, feat_combo))
                        feat_combos.append(verb_)
                    feature_combinations.append(feat_combos)
            
            # if the form to analyse has no morphological features,
            # it doesn't make sense to compute grammatical features
            if num_feats == 0:
                break
            
            # German
            # An infinitive preceding a form of "haben" could be a substitute infinitive, i.e. an 
            # infinitive in the function of a perfect participle. We add the participle analysis.
            if (
                len(feature_combinations) > 1 
                and feature_combinations[-1][0]["lemma"] == "haben" 
                and "Inf" in [feat_combos["feats"]["VerbForm"] for feat_combos in feature_combinations[-2] if "VerbForm" in feat_combos["feats"]]
            ):
                verb = feature_combinations[-2][0]
                feature_combinations[-2].append({
                    "form": verb["form"], 
                    "lemma": verb["lemma"], 
                    "id": verb["id"], 
                    "head": verb["head"], 
                    "upos": verb["upos"], 
                    "feats": {"VerbForm": "Part", "Aspect": "Perf"}
                })
            
            feature_combinations = list(itertools.product(*feature_combinations))

            # map morphological features to grammatical features
            analyses = set()
            for feature_combination in feature_combinations:
                analyses.update(lookup(feature_combination, language))
            analyses = [unhash_feats(analysis) for analysis in analyses]
            
            # if no analysis is found and only one verb is left,
            # treat the verb as simple form
            if len(analyses) == 0 and len(verbs) == 1:
                analyses = [feature_combination[0]["feats"] for feature_combination in feature_combinations]
            
            for analysis in analyses:
                for feat in analysis:
                    analysis[feat] = set(analysis[feat])
                
            if len(analyses) > 0:

                for i, analysis in enumerate(analyses):
                    if "VerbForm" in analysis and len(analysis["VerbForm"]) > 0:
                        
                        # unification of infinite forms:
                        # Inf   -> Inf Verb
                        # Vnoun -> Inf Noun
                        # Part  -> Part Adj
                        # Conv  -> Part Adv
                        if "Inf" in analysis["VerbForm"]:
                            analysis["VerbForm_"] = set(["Verb"])
                        elif "Part" in analysis["VerbForm"]:
                            analysis["VerbForm_"] = set(["Adj"])
                        elif "Vnoun" in analysis["VerbForm"]:
                            analysis["VerbForm"].remove("Vnoun")
                            analysis["VerbForm"].add("Inf")
                            analysis["VerbForm_"] = set(["Noun"])
                        elif "Conv" in analysis["VerbForm"]:
                            analysis["VerbForm"].remove("Conv")
                            analysis["VerbForm"].add("Part")
                            analysis["VerbForm_"] = set(["Adv"])
                        
                        if reduce_infinite_forms and "Fin" not in analysis["VerbForm"]:
                            analyses[i] = {"VerbForm" : analysis["VerbForm"]}
                
                # filter by voice
                voices = set()
                for verb in verbs:
                    if "Voice" in verb["feats"]:
                        voices.update(verb["feats"]["Voice"])
                    if verb["deprel"].endswith(":pass"):
                        voices.add("Pass")
                filtered_analyses = [analysis for analysis in analyses if not ("Voice" in analysis and len(analysis["Voice"].intersection(voices)) == 0)]
                if len(filtered_analyses) > 0:
                    analyses = filtered_analyses
                
                # filter by underspecified values
                # - if no voice is specified, assume that it is "Act"
                # - if no mood is specified, assume that it is "Ind"
                defaults = [("Voice", "Act"), ("Mood", "Ind")]#, ("Aspect", "Imp"), ("Tense", "Pres"), ("VerbForm", "Inf")]
                for name, val in defaults:
                    filtered_analyses = [analysis for analysis in analyses if not (name in analysis and val not in analysis[name])]
                    if len(filtered_analyses) > 0:
                        analyses = filtered_analyses

                if len(analyses) > 1:
                    # the combined analysis contains those features
                    # for which all remaining analyses have a common value or no value
                    combined_analysis = {}
                    for analysis in analyses:
                        for feat in analysis:
                            vals = set(analysis[feat])
                            try:
                                combined_analysis[feat].intersection_update(vals)
                            except KeyError:
                                combined_analysis[feat] = vals
                    combined_analysis = {feat : combined_analysis[feat] for feat in combined_analysis if len(combined_analysis[feat]) > 0}
                    analysis = combined_analysis
                else:
                    analysis = analyses.pop()
                
                # add modality
                modalities = set()
                for verb in verbs:
                    if verb["category"] == "mod":
                        for cat in lang_data["mod"]:
                            if verb["lemma"] in lang_data["mod"][cat]:
                                modalities.add(str(cat))
                modalities.discard("AUX")
                if len(modalities) > 0:
                    analysis["Modality"] = modalities
                
                return analysis

            verbs = verbs[1:]

    return {}


def hash_feats(feats):
    """Map a feature dict to a string.

    Args:
        dict of str:(list of str): Feature dict.
    
    Returns:
        str: Stringified feature dict.
    
    """
    return json.dumps(feats, sort_keys=True)


def unhash_feats(feats):
    """Map a feature dict's string representation back to the original feature dict.

    Args:
        str: Stringified feature dict.
    
    Returns:
        dict of str:(list of str): Feature dict.
    
    """
    return json.loads(feats)


def is_match(feat_list1, feat_list2, language):
    """Checks whether two feature dicts match.
        Two feature dicts match iff both
            - the lemmas belong to the same class
            - the value of each morphological feature is identical
                (unspecified features match with any value)

    Args:
        feat_list1 (dict of str:obj): Feature dict.
        feat_list2 (dict of str:obj): Feature dict.
        language (str): Language code.
    
    Returns:
        boolean: True iff the feature dicts match.
    
    """
    aux_verbs = LANG_DATA[language]["aux"]
    for feats1, feats2 in zip(feat_list1, feat_list2):
        lemma1 = feats1["lemma"]
        lemma2 = feats2["lemma"]
        if not (lemma1 == lemma2 or (lemma1 in aux_verbs and lemma2 in aux_verbs[lemma1]) or (lemma2 in aux_verbs and lemma1 in aux_verbs[lemma2])):
            return False
        feats1 = feats1["feats"]
        feats2 = feats2["feats"]
        keys = set(feats1.keys()).intersection(feats2.keys())
        for key in keys:
            val1 = feats1[key]
            val2 = feats2[key]
            if val1 != val2:
                return False
    return True


def lookup(feature_combination, language):
    """Look up a feature combination in the verb forms of a language.

    Args:
        feature_combination (list of (dict of str:obj)): List of morpological feature dicts.
        language (str): The language code.
    
    Returns:
        set of str: Stringified analyses of the feature combination.
    
    """
    analyses = set()
    lang_table = LANG_TABLE[language]
    for verbs, analysis in lang_table:
        for combination in itertools.permutations(feature_combination):
            if len(verbs) != len(combination):
                break
            elif is_match(verbs, combination, language):
                analyses.add(hash_feats(analysis))
                break
    """
    i = 0
    line = []
    for word in feature_combination:
        i += 1
        feats = word["feats"]
        if True or word["lemma"] != "#main#":
            line.append(word["lemma"])
        for feat in ["VerbForm", "Tense", "Aspect", "Mood", "Voice"]:
            if feat in feats:
                line.append(feats[feat])
            else:
                line.append("_")
    print("\t".join(line))
    """
    return analyses


def extract_verb_feats(compound_verb, language, grammatical_features):
    """Extract verb-form features.
    
    Args:
        compound_verb (list of `Token`): A compound verb.
        language (str): The language code.
        grammatical_features (boolean): If True, grammatical features are computed for the compound verb;
            if False, morphological features are used instead.
    
    Returns:
        dict of str:boolean: The feature dict.

    """
    feats = {}
    if grammatical_features:
        gramm_feats = compute_grammatical_feats(compound_verb, language)
        for feat_name in gramm_feats:
            feat_vals = gramm_feats[feat_name]
            for feat_val in feat_vals:
                feats[feat_name + "=" + feat_val] = True
    else:
        for token in compound_verb:
            prefix = unify_tags(token["upos"])
            morph_feats = token["feats"]
            if morph_feats is not None:
                for feat_name in morph_feats:
                    feat_val = morph_feats[feat_name]
                    feat_name, feat_vals = unify_morphs(feat_name, feat_val)
                    if feat_name in VERBAL_FEATS:
                        for feat_val in feat_vals:
                            feats[prefix + "_" + feat_name + "=" + feat_val] = True
    return feats


def extract_NP_feats(NP):
    """Extract NP features.
    
    Args:
        NP (`Unit`): An NP.
    
    Returns:
        dict of str:boolean: The feature dict.

    """
    feats = {}
    
    # dependency relation and tag
    prefix = unify_deps(deprel(NP.root))
    feats[prefix] = True
    feats[prefix + "_" + unify_tags(NP.root["upos"])] = True

    for token in NP.tokens:
        morph_feats = token["feats"]
        if morph_feats is not None:
            for feat_name in morph_feats:
                feat_val = morph_feats[feat_name]
                feat_name, feat_vals = unify_morphs(feat_name, feat_val)
                if feat_name in LEXICAL_FEATS or feat_name in NOMINAL_FEATS:
                    # case, person, number and gender are agreement features, 
                    # i.e. they should be the same in the entire NP; for these features, 
                    # we select the values from the NP's head only (+ particles attached to the head)
                    # other features are taken from all words in the NP
                    if feat_name not in ["Case", "Person", "Number", "Gender"] or token == NP.root or (token["upos"] == "PART" and token["head"] == NP.root["id"]):
                        for feat_val in feat_vals:
                            feats[prefix + "_" + feat_name + "=" + feat_val] = True

    """
    # case, number and gender are agreement features,
    # only one value should be allowed
    cng = {"Case" : {}, "Number" : {}, "Gender" : {}}
    
    for token in NP.tokens:
        morph_feats = token["feats"]
        if morph_feats is not None:
            for feat_name in morph_feats:
                feat_val = morph_feats[feat_name]
                feat_name, feat_vals = unify_morphs(feat_name, feat_val)
                if feat_name in LEXICAL_FEATS or feat_name in NOMINAL_FEATS:
                    for feat_val in feat_vals:
                        
                        if feat_name in cng:
                            # collect value for agreement feature
                            # exclude non-head nouns (usually compounds, names, appositions etc.)
                            if token == NP.root or unify_tags(token["upos"]) not in ["NOUN", "PROPN"]:
                                if feat_val not in cng[feat_name]:
                                    cng[feat_name][feat_val] = 0
                                cng[feat_name][feat_val] += 1.0/len(feat_vals)
                        
                        else:
                            # add value for any other feature
                            feats[prefix + "_" + feat_name + "=" + feat_val] = True

    # for the agreement features, add the value that most tokens showed
    for feat_name in cng:
        try:
            max_val = max(cng[feat_name].values())
            feat_vals = [feat_val for feat_val in cng[feat_name] if cng[feat_name][feat_val] == max_val]
            for feat_val in feat_vals:
                feats[prefix + "_" + feat_name + "=" + feat_val] = True
        except ValueError:
            pass
    """
    
    return feats


def extract_disc_feats(token):
    """Extract discourse/connective features.
    
    Args:
        token (`Token`): A token.
    
    Returns:
        dict of str:boolean: The feature dict.

    """
    feats = {}
    prefix = unify_deps(deprel(token))
    feats[prefix] = True
    feats[prefix + "_" + unify_tags(token["upos"])] = True
    return feats


def aggregate_feats(feats, _feats, prefix):
    """Add features to a feature dict.
        Use a prefix for the new features to not overwrite existing features with the same name.
    
    Args:
        feats (dict of str:boolean): Feature dict.
        _feats (dict of str:boolean): Features to be included.
        prefix (str): Prefix for the new features.
    
    Returns:
        dict of str:boolean: The updated feature dict.

    """
    for feat_name in _feats:
        feats[prefix + "_" + feat_name] = _feats[feat_name]
    return feats


def split_camel_case(val):
    """Split a string of camel-case values.
        Example: "DatAcc" -> ["Dat", "Acc"]
    
    Args:
        val (str): The string.
    
    Returns:
        list of str: List of values.
    
    """
    return re.sub("([A-Z][a-z]+)", r" \1", re.sub("([A-Z]+)", r" \1", val)).split()


def unify_morphs(feat_name, feat_val):
    """Unify small variants in morphology feature-value pairs.
        Feature subtypes are removed.

    Args:
        feat_name (str): Name of the feature.
        feat_val (str): Value of the feature.
            Sometimes, these are several values separated by comma.
    
    Returns:
        str: Name of the feature.
        list of str: Values of the feature.
    
    """
    feat_name = feat_name.split("[")[0]
    feat_vals = feat_val.split(",")
    feat_vals = sum([split_camel_case(val) for val in feat_vals], [])
    
    # unify `Person` values
    if feat_name == "Person":
        feat_vals_ = []
        for val in feat_vals:
            if val in ["1", "First"]:
                feat_vals_.append("1")
            elif val in ["2", "Second"]:
                feat_vals_.append("2")
            elif val in ["3", "Third"]:
                feat_vals_.append("3")
        feat_vals = feat_vals_
    
    return feat_name, feat_vals


def unify_tags(foo):
    """Map non-UPOS tags to UPOS tags.
        - maps CTB tags to UPOS tags

    Args:
        foo (str): A tag from any tagset.
    
    Returns:
        str: The corresponding UPOS tag.
    
    """
    ctb_to_ud = {
        "AD" : "ADV", 
        "AS" : "PART", 
        "BA" : "PART", 
        "CC" : "CCONJ", 
        "CD" : "NUM", 
        "CS" : "SCONJ", 
        "DEC" : "PART", 
        "DEG" : "PART", 
        "DER" : "PART", 
        "DEV" : "PART", 
        "DT" : "DET", 
        "ETC" : "X", 
        "FW" : "X", 
        "IJ" : "INTJ", 
        "JJ" : "ADJ", 
        "LB" : "PART", 
        "LC" : "PART", 
        "M" : "NOUN", 
        "MSP" : "PART", 
        "NN" : "NOUN", 
        "NR" : "PROPN", 
        "NT" : "NOUN", 
        "OD" : "NUM", 
        "ON" : "X", 
        "P" : "ADP", 
        "PN" : "PRON", 
        "PU" : "PUNCT", 
        "SB" : "PART", 
        "SP" : "PART", 
        "VA" : "ADJ", 
        "VC" : "AUX", 
        "VE" : "VERB", 
        "VV" : "VERB"
    }
    if foo in ctb_to_ud:
        return ctb_to_ud[foo]
    
    if foo not in UNIVERSAL_TAGS:
        return "X"
    
    return foo


def unify_deps(foo):
    """Map non-UD relations to UD relations.
        - maps Stanford relations to UD relations

    Args:
        foo (str): A relation from any inventory.
    
    Returns:
        str: The corresponding UD relation.
    
    """
    stanford_to_ud = {
        "acomp" : "xcomp",
        "agent" : "obl",
        "attr" : "xcomp",
        "auxpass" : "aux",
        "csubjpass" : "csubj",
        "dative" : "iobj",
        "dobj" : "obj",
        "intj" : "discourse",
        "meta" : "dep",
        "neg" : "advmod",
        "npadvmod" : "obl",
        "nsubjpass" : "nsubj",
        "oprd" : "xcomp",
        "pcomp" : "ccomp",
        "pobj" : "obl",
        "poss" : "det",
        "preconj" : "cc",
        "predet" : "det",
        "prep" : "case",
        "prt" : "compound",
        "quantmod" : "det",
        "relcl" : "acl"
    }
    if foo in stanford_to_ud:
        return stanford_to_ud[foo]
    
    if foo not in UNIVERSAL_DEPS:
        return "dep"
    
    return foo


def unify_punct(foo):
    """Unify punctuation across languages.
        - replaces full-width characters with half-with characters
        - replaces "。" with "."
        - replaces "、" with ","
        - unifies double quotes
        - unifies single quotes

    Args:
        foo (str): A string containing punctuation.
    
    Returns:
        str: A new string with unified punctuation.
    
    """
    foo = unicodedata.normalize("NFKC", foo)
    foo = foo.replace(u'。', '.').replace(u'、', ',')
    
    double_quotes = [u'„', u'“', u'”', u'»', u'«', u'『', u'』']
    for q in double_quotes:
        foo = foo.replace(q, '"')
    
    single_quotes = [u'‚', u'‘', u'’', u'›', u'‹', u'「', u'」']
    for q in single_quotes:
        foo = foo.replace(q, "'")
    
    hyphens = [[u'‐', u'‑'], [u'‒', u'–'], [u'—', u'―']]
    for i, dashes in enumerate(hyphens):
        for dash in dashes:
            foo = foo.replace(dash, "-" * (i+1))
    
    for char in foo:
        if char not in string.punctuation:
            return None
    
    return foo