import json
import matplotlib.pyplot as plt
import os
import pickle
import random
from hypopt import GridSearch
from sklearn import tree
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction import DictVectorizer
from sklearn.feature_selection import f_classif, SelectKBest, VarianceThreshold
from sklearn.metrics import f1_score, make_scorer
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import ComplementNB
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier

from extract_features import extract_feats_with_context, extract_feats_connective
from file_reader import read_file
from split_units_ud import *


def prepare_data(documents_languages, grammatical_features):
    """Prepare data for classifier training and prediction.
        Splits sentences into clauses and extracts features from clauses and connectives.

    Args:
        documents_languages (list of (list of (list of (dict of str:obj)),str)): List of document-language pairs.
            (For monolingual training, the language is always the same.)
            A document is a list of sentences. A sentence is a list of tokens. A token is a CoNLL dictionary.
    
    Returns:
        list of (dict of str:bool): Feature dictionary for each clause.
        list of bool: Class of each clause.
        list of (dict of str:obj): Feature dictionary for each connective.
        list of int: Length of each connective.
    
    """
    X = []
    Y = []
    X_conn = []
    Y_conn = []
    connectives = []
    for document, language in documents_languages:
        clauses = []
        for sentence in document:
            sentence = unit_from_sentence(sentence, language)
            clauses.extend(split_into_clauses(sentence))
        for i, clause in enumerate(clauses):
            feats = extract_feats_with_context(clauses, i, grammatical_features)
            X.append(feats)
            seg_start = False
            token = clause.tokens[0]
            if "misc" in token and token["misc"] is not None:
                misc = token["misc"]
                if "BeginSeg" in misc and misc["BeginSeg"] == "Yes":
                    # non-PDBT style
                    seg_start = True
                elif "Seg" in misc and misc["Seg"] == "B-Conn":
                    # PDBT style
                    seg_start = True
                    connective = get_connective_tokens(clause)
                    n = len(connective)
                    if n <= 5:
                        connectives.append(connective + clause.tokens[n:5])
                        Y_conn.append(n)
            Y.append(seg_start)
    for connective in connectives:
        X_conn.append(extract_feats_connective(connective))
    return X, Y, X_conn, Y_conn


def get_connective_tokens(clause):
    """Get all tokens that belong to a connective at a clause start.

    Args:
        clause (`Unit`): The clause.
    
    Returns:
        list of (dict of str:obj): List of tokens.
    
    """
    connective = [clause.tokens[0]]
    if len(clause.tokens) > 0:
        for token in clause.tokens[1:]:
            if "misc" in token and token["misc"] is not None and "Seg" in token["misc"] and token["misc"]["Seg"] == "I-Conn":
                connective.append(token)
            else:
                break
    return connective


def train(path_to_model, prepared_data, validation_data, save_trees=False):
    """Train a model.

    Args:
        path_to_model (str): Path where the model should be saved.
        prepared_data (list of (list of (list of (dict of str:obj)),str)): Training data, prepared with `prepare_data`.
        validation_data (list of (list of (list of (dict of str:obj)),str)): Development data, prepared with `prepare_data`.
        save_trees (bool): Iff True, images of the trained decision tree are saved.
    
    """
    X, Y, X_conn, Y_conn = prepared_data
    if validation_data is not None:
        X_dev, Y_dev, _, _ = validation_data

    dv = DictVectorizer()
    dv.fit(X)
    X = dv.transform(X)

    fs = VarianceThreshold(-1)
    fs.fit(X, Y)
    X = fs.transform(X)

    if validation_data is not None:
        gs = GridSearch(model=DecisionTreeClassifier(random_state=0), param_grid=[{'min_samples_leaf': [1, 2, 5, 10, 15, 20], 'max_depth': [5, 10, 15, 20, 25, None]}])
        gs.fit(X, Y, fs.transform(dv.transform(X_dev)), Y_dev, verbose=False)
        json.dump(gs.best_params, open(path_to_model + ".params.txt", "w"), sort_keys=True)
        clf = DecisionTreeClassifier(random_state=0, min_samples_leaf=gs.best_params['min_samples_leaf'], max_depth=gs.best_params['max_depth'])
    else:
        json.dump({'min_samples_leaf': 20, 'max_depth': None}, open(path_to_model + ".params.txt", "w"), sort_keys=True)
        clf = DecisionTreeClassifier(random_state=0, min_samples_leaf=20, max_depth=None)
    
    clf.fit(X, Y)

    if save_trees:
        plt.figure(figsize=(120,40))
        tree.plot_tree(clf, max_depth=10, feature_names=dv.feature_names_, class_names=["False", "True"], fontsize=10)
        plt.savefig(path_to_model + ".tree.png")
        plt.close()

    if len(X_conn) > 0:
        dv_conn = DictVectorizer()
        dv_conn.fit(X_conn)
        X_conn = dv_conn.transform(X_conn)

        clf_conn = DecisionTreeClassifier(random_state=0)
        clf_conn.fit(X_conn, Y_conn)

        if save_trees:
            plt.figure(figsize=(120,40))
            tree.plot_tree(clf_conn, max_depth=10, feature_names=dv_conn.feature_names_, class_names=["1", "2", "3", "4", "5"], fontsize=10)
            plt.savefig(path_to_model + ".tree_conn.png")
            plt.close()

    else:
        dv_conn = None
        clf_conn = None

    with open(path_to_model, 'wb') as f:
        pickle.dump((clf, dv, fs, clf_conn, dv_conn), f)


def test(path_to_model, prepared_data):
    """Test a model.

    Args:
        path_to_model (str): Path where the model is saved.
        prepared_data (list of (list of (list of (dict of str:obj)),str)): Test data, prepared with `prepare_data`.
    
    """
    with open(path_to_model, 'rb') as f:
        clf, dv, fs, clf_conn, dv_conn = pickle.load(f)
    X, Y, X_conn, Y_conn = prepared_data
    print(f1_score(Y, clf.predict(fs.transform(dv.transform(X))), labels=[True]))
    if clf_conn is not None and len(X_conn) > 0:
        print(f1_score(Y_conn, clf_conn.predict(dv_conn.transform(X_conn)), average="micro"))
    else:
        print(None)


def predict(path_to_model, path_to_file, grammatical_features, path_to_output=None):
    """Use a model to make predictions.

    Args:
        path_to_model (str): Path where the model is saved.
        path_to_file (str): Path to the input file.
        grammatical_features (bool): If True, grammatical features are computed for the compound verb;
            if False, morphological features are used instead.
        path_to_output (str): Path to the output file.
            If None, the output file is saved in `predictions/...`.
    
    Returns:
        str: Path to the output file.
    
    """
    with open(path_to_model, 'rb') as f:
        clf, dv, fs, clf_conn, dv_conn = pickle.load(f)
    documents = read_file(path_to_file)
    languages = [os.path.basename(path_to_file).split(".")[0]] * len(documents)
    documents_languages = list(zip(documents, languages))
    seg_start_tokens = []
    seg_inner_tokens = []
    for document, language in documents_languages:
        clauses = []
        for sentence in document:
            sentence = unit_from_sentence(sentence, language)
            clauses.extend(split_into_clauses(sentence))
        for i, clause in enumerate(clauses):
            feats = extract_feats_with_context(clauses, i, grammatical_features)
            seg_start = clf.predict(fs.transform(dv.transform([feats])))[0]
            if seg_start:
                seg_start_tokens.append(clause.tokens[0])
                if clf_conn is not None:
                    feats_conn = extract_feats_connective(clause.tokens[:5])
                    n = clf_conn.predict(dv_conn.transform([feats_conn]))[0]
                    seg_inner_tokens.extend(clause.tokens[1:n])
    sentences = []
    for document, language in documents_languages:
        for sentence in document:
            for token in sentence:
                token["misc"] = {}
                if token in seg_start_tokens:
                    if ".pdtb." in path_to_file:
                        token["misc"]["Seg"] = "B-Conn"
                    else:
                        token["misc"]["BeginSeg"] = "Yes"
                elif token in seg_inner_tokens:
                    if ".pdtb." in path_to_file:
                        token["misc"]["Seg"] = "I-Conn"
                if len(token["misc"]) == 0:
                    token["misc"] = None
            sentences.append(sentence)
    if path_to_output is None:
        path = path_to_file.split(os.path.sep)
        output_dir = os.path.join("predictions", *path[1:-1])
        output_file = path[-1]
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        path_to_output = os.path.join(output_dir, output_file)
    with open(path_to_output, 'w') as f:
        for sentence in sentences:
            f.write(sentence.serialize())
    return path_to_output
