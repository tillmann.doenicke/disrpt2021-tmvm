import io
import os
from conllu import parse, parse_incr


def read_file(path_to_file):
    """Read a CoNLL-U file.

    Args:
        path_to_file (str): Path to file.
    
    Returns:
        list of (list of (dict of str:obj)): List of documents.
            A document is a list of sentences. A sentence is a list of tokens. A token is a CoNLL dictionary.
    
    """
    documents = []
    with open(path_to_file, "r") as f:
        
        data = f.read()

        # each file consists of documents
        # each document consists of sentences
        sentences = []
        for sentence in parse_incr(io.StringIO(data)):
            metadata = sentence.metadata
            if "newdoc id" in metadata:
                if len(sentences) > 0:
                    documents.append(sentences)
                    sentences = []
            sentences.append(sentence)
        if len(sentences) > 0:
            documents.append(sentences)
    
    return documents


def read_data(ending, langs=[], frameworks=[], names=[], exclude=[]):
    """Read several files from `data`.

    Args:
        ending (str): The suffix of the files to read, e.g. `_train.conllu`.
        langs (list of str): The languages to consider.
            If empty, all languages are considered.
        frameworks (list of str): The frameworks to consider.
            If empty, all frameworks are considered.
        names (list of str): The names to consider.
            If empty, all names are considered.
        exclude (list of str): List of datasets to exclude.
    
    Returns:
        (list of (list of (list of (dict of str:obj)),str)): List of document-language pairs.
            A document is a list of sentences. A sentence is a list of tokens. A token is a CoNLL dictionary.
    
    """
    documents_languages = []
    
    path_to_data = "data"
    corpora = sorted(os.listdir(path_to_data))
    
    # iterate over all corpora in "data"
    for corpus in corpora:
        if corpus in exclude:
            continue
        path_to_corpus = os.path.join(path_to_data, corpus)
        if os.path.isdir(path_to_corpus):

            # check if corpus matches specified conditions
            info = corpus.split(".")
            if (
                (len(langs) == 0 or info[0] in langs) and 
                (len(frameworks) == 0 or info[1] in frameworks) and 
                (len(names) == 0 or info[2] in names)
            ):

                # iterate over all files in a corpus
                files = sorted(os.listdir(path_to_corpus))
                for file in files:
                    
                    # open the file if it matches the desired ending
                    if file.endswith(ending):
                        path_to_file = os.path.join(path_to_corpus, file)
                        file_documents = read_file(path_to_file)
                        file_languages = [info[0]] * len(file_documents)
                        documents_languages.extend(list(zip(file_documents, file_languages)))
    
    return documents_languages