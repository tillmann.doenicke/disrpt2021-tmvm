from lang_trans.arabic import buckwalter

from resources.inflection.lang_data import LANG_DATA


class Unit():
    """A class to represent syntactic units.

    """
    def __init__(self, root, tokens, language):
        """`__init__` method of the class `Unit`.

        Args:
            root (`Token`): The syntactically highest token in the unit.
            tokens (iterable of `Token`): All tokens in the unit.
            language (str): The language code of the unit (for multilingual training).
        
        """
        self.root = root         # root token
        self.tokens = tokens     # all tokens
        self.language = language # language
        self.prec_punct = []     # preceding punctuation
        self.succ_punct = []     # succeeding punctuation
        self.parent_unit = None  # superordinate syntactic unit
    
    
    def __str__(self):
        """`__str__` method of the class `Unit`.
        """    
        return " ".join([token["form"] for token in self.prec_punct] + [token["form"] for token in self.tokens] + [token["form"] for token in self.succ_punct])
    
    
    def __repr__(self):
        """`__repr__` method of the class `Unit`.
        """    
        return self.__str__()


def unit_from_sentence(sentence, language):
    """Convert a list of tokens into a `Unit` object.

    Args:
        sentence (list of (dict of str:obj)): List of tokens.
            A token is a CoNLL dictionary.
        language (str): The language.
    
    Returns:
        `Unit`: Sentence as `Unit` object.
    
    """
    root = None
    for token in sentence:
        if token["head"] == 0:
            root = token
            break
    return Unit(root, sentence, language)


def split_into_NPs(unit):
    """Split a syntactic unit (a clause) into NPs using dependency relations.

    Args:
        unit (`Unit`): The clause.
    
    Returns:
        list of `Unit`: List of NPs.
    
    """    
    relations = ["nsubj", "obj", "iobj", "obl", "nmod"] + ["nsubjpass", "dobj", "pobj", "agent", "dative", "npadvmod", "oprd"]
    return split_into_subunits(unit, relations)


def split_into_clauses(unit):
    """Split a syntactic unit (a sentence) into clauses using dependency relations.

    Args:
        unit (`Unit`): The sentence.
    
    Returns:
        list of `Unit`: List of clauses.
    
    """    
    relations = ["root", "parataxis", "list", "xcomp", "ccomp", "csubj", "acl", "advcl", "vocative", "discourse"] + ["csubjpass", "pcomp", "relcl", "intj"]
    return split_into_subunits(unit, relations)


def split_into_subunits(unit, relations):
    """Split a syntactic unit into subunits using dependency relations,
        e.g. a sentence into clauses or a clause into phrases.
    
    Args:
        unit (`Unit`): The unit.
        relations (list of str): List of dependency relations that are heads of subunits.
    
    Returns:
        list of `Unit`: List of subunits.
    
    """
    subunits = {}

    lang_data = LANG_DATA[unit.language]
    mod_lemmas = set([lemma for cat in lang_data["mod"] for lemma in lang_data["mod"][cat]])
    
    # find all heads of subunits
    for token in unit.tokens:
        head_token = get_head(unit, token)
        if (
            (deprel(token) in relations and lemma(head_token, unit.language) not in mod_lemmas) or 
            (deprel(token) == "conj" and deprel(head_token) in relations)
        ):
            # 1. a modal verb cannot be the head of a clause (but should be part of that clause instead)
            # 2. conjuncts are only clauses if they are conjuncted with a clause
            subunits[token["id"]] = Unit(token, [], unit.language)

    # assign all tokens to heads of subunits
    for token in unit.tokens:
        current_token = token
        while True:
            if current_token["id"] in subunits:
                subunits[current_token["id"]].tokens.append(token)
                break
            head_token = get_head(unit, current_token)
            if current_token == head_token:
                break
            else:
                current_token = head_token
    
    subunits = subunits.values()

    # handle punctuation, set parent unit
    for subunit in subunits:
        subunit.tokens = sorted(subunit.tokens, key=lambda token: unit.tokens.index(token))
        while deprel(subunit.tokens[0]) == "punct":
            subunit.prec_punct.append(subunit.tokens.pop(0))
        while deprel(subunit.tokens[-1]) == "punct":
            subunit.succ_punct.append(subunit.tokens.pop())
        subunit.parent_unit = unit

    subunits = sorted(subunits, key=lambda subunit: unit.tokens.index(subunit.tokens[0]))

    return subunits


def lemma(token, language):
    """Return the lemma of a token.
        In Persian, this is only the past stem of a verb.

    Args:
        token (`Token`): The token.
    
    Returns:
        str: The lemma.
            Persian lemmas are transliterated with Latin characters.
    
    """
    lemma = token["lemma"]
    if language == "fas":
        lemma = lemma.split("#")[0]
        lemma = buckwalter.transliterate(lemma)
        lemma = "".join([(c if ord(c) < 128 else "?") for c in lemma])
    return lemma


def deprel(token):
    """Return the dependency relation of a token, without the subtype.

    Args:
        token (`Token`): The token.
    
    Returns:
        str: The dependecy relation without subtype.
    
    """
    return token["deprel"].split(":")[0].lower()


def get_head(unit, current_token):
    """Return the head token of a given token in a syntactic unit.

    Args:
        unit (`Unit`): The unit.
        current_token (`Token`): The token.
    
    Returns:
        `Token`: The head of the token.
            Returns the given token if no head is found.
    
    """
    for token in unit.tokens:
        if current_token["head"] == token["id"]:
            return token
    return current_token
