import os
import sys

from classifier import prepare_data, train
from file_reader import read_file

if __name__ == "__main__":
    args = sys.argv
    if len(args) > 1:
        path_to_train_file = args[1]
        path_to_dev_file = path_to_train_file.replace("_train", "_dev")

        train_documents = read_file(path_to_train_file)
        dev_documents = read_file(path_to_dev_file)

        grammatical_features = True
        language = os.path.basename(path_to_train_file).split("_")[0].split(".")[0]
        train_data = prepare_data(list(zip(train_documents, [language] * len(train_documents))), grammatical_features)
        dev_data = prepare_data(list(zip(dev_documents, [language] * len(dev_documents))), grammatical_features)

        path_to_model = os.path.join("models", os.path.basename(path_to_train_file) + ".pickle")
        train(path_to_model, train_data, dev_data)
    
    else:
        raise Exception("You must specify a training file.")