import csv
import os


"""
`LANG_DATA` stores grammatical information for each language.
    "aux": a dictionary of auxiliary verbs that are used in composite verb forms.
        "AUX1" ... "AUX4" represent different groups of auxiliaries (for cases in which 
        A composite verb form could be built with different auxiliary verbs, depending 
        on the main verb or simply because of synonymous auxiliaries). There is no meaning
        in the number following "AUX" but I followed this rough scheme:
            - "AUX1" contains auxiliaries used to build primary compound tense-aspect forms,
                e.g. perfect forms in English
            - "AUX2" contains auxiliaries controlling voice or mood,
                e.g. passive voice in English
            - "AUX3" contains auxiliaries used to build secondary compound tense-aspect forms,
                e.g. future tense in English
            - "AUX4" contains auxiliaries functioning as copula in compound forms,
                e.g. progressive aspect in English
            - "AUX5" contains auxiliaries used in grammaticalised verbal compound constructions,
                e.g. light verbs in Persian
    "mod": a dictionary of modal verbs (those will be excluded from composite verb forms).
        The modal verbs are grouped into three categories according to their semantic function:
            - "AUX": "empty" verbs that don't have any meaning but can take inflectional features.
                e.g. "do" in English
            - "OBL": verbs associated with obligation/necessity,
                e.g. "must" and "shall" in English
            - "POS": verbs associated with possibility/ability/permission,
                e.g. "can" and "may" in English
            - "VOL": verbs associated with volition/prediction,
                e.g. "wollen" `want to` in German
    "OV": Whether the language's basic word order is object-verb (True) or verb-object (False).
        (This is order usually identical to the basic verb order: "V_subordinate-V_head".)
"""
LANG_DATA = {
    "deu" : {
        "aux" : {
            "AUX1" : ["haben", "sein"],
            "AUX2" : ["sein", "werden"],
            "AUX3" : ["werden"],
            "AUX4" : ["sein"],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : [],
            "OBL" : ["müssen", "sollen"],
            "POS" : ["dürfen", "können", "mögen"],
            "VOL" : ["wollen"]
        },
        "OV" : True
    },
    "eng" : {
        "aux" : {
            "AUX1" : ["be", "have"],
            "AUX2" : ["be", "get"],
            "AUX3" : ["will", "would"],
            "AUX4" : ["be"],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : ["do"],
            "OBL" : ["must", "shall", "should"],
            "POS" : ["can", "could", "may", "might"],
            "VOL" : []
        },
        "OV" : False
    },
    "eus" : {
        "aux" : {
            "AUX1" : ["*edun", "izan"],
            "AUX2" : ["izan"],
            "AUX3" : ["*edin", "*ezan"],
            "AUX4" : ["egon", "izan"],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : [],
            "OBL" : [],
            "POS" : [],
            "VOL" : []
        },
        "OV" : True
    },
    "fas" : {
        "aux" : {
            "AUX1" : ["Ast", "bwd", "hst"],
            "AUX2" : ["$d"],
            "AUX3" : ["xwAst"],
            "AUX4" : ["dA$t"],
            "AUX5" : ["?rd"]
        },
        "mod" : {
            "AUX" : [],
            "OBL" : ["bA?st"],
            "POS" : ["twAn", "twAnst"],
            "VOL" : []
        },
        "OV" : True
    },
    "fra" : {
        "aux" : {
            "AUX1" : ["avoir", "être"],
            "AUX2" : ["être"],
            "AUX3" : [],
            "AUX4" : [],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : [],
            "OBL" : ["devoir"],
            "POS" : ["pouvoir"],
            "VOL" : ["vouloir"]
        },
        "OV" : False
    },
    "nld" : {
        "aux" : {
            "AUX1" : ["hebben", "zijn"],
            "AUX2" : ["worden"],
            "AUX3" : ["zullen"],
            "AUX4" : ["zijn"],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : [],
            "OBL" : ["moeten"],
            "POS" : ["kunnen", "mogen"],
            "VOL" : ["willen"]
        },
        "OV" : True
    },
    "por" : {
        "aux" : {
            "AUX1" : ["haver", "ter"],
            "AUX2" : ["ser"],
            "AUX3" : ["ir"],
            "AUX4" : ["estar"],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : [],
            "OBL" : ["dever"],
            "POS" : ["poder"],
            "VOL" : []
        },
        "OV" : False
    },
    "rus" : {
        "aux" : {
            "AUX1" : ["быть"],
            "AUX2" : [],
            "AUX3" : [],
            "AUX4" : [],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : [],
            "OBL" : ["приходиться"],
            "POS" : ["мочь"],
            "VOL" : ["хотеть", "хотеться"]
        },
        "OV" : False
    },
    "spa" : {
        "aux" : {
            "AUX1" : ["haber"],
            "AUX2" : ["estar", "ser"],
            "AUX3" : [],
            "AUX4" : ["estar"],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : [],
            "OBL" : ["deber"],
            "POS" : ["poder"],
            "VOL" : []
        },
        "OV" : False
    },
    "tur" : {
        "aux" : {
            "AUX1" : [],
            "AUX2" : [],
            "AUX3" : [],
            "AUX4" : ["i"],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : [],
            "OBL" : [],
            "POS" : [],
            "VOL" : []
        },
        "OV" : True
    },
    "zho" : {
        "aux" : {
            "AUX1" : [],
            "AUX2" : ["被"],
            "AUX3" : ["让", "叫", "请", "使"] + ["讓", "請"],
            "AUX4" : [],
            "AUX5" : []
        },
        "mod" : {
            "AUX" : [],
            "OBL" : ["应当", "应该", "得", "必得", "必要", "必须", "该"] + ["必須", "應當", "應該", "該"],
            "POS" : ["会", "可以", "敢", "能", "能够"] + ["會", "能夠"],
            "VOL" : ["肯"]
        },
        "OV" : False
    }
}


"""
`LANG_TABLE` maps a language code to a list of tuples, where each tuple consists of
    1) a list of dictionaries (with keys "lemma" and "feats") representing verbs, and 
    2) the grammatical anylsis of this combination.
The data is read from `table.csv`.
"""
LANG_TABLE = {}
with open(os.path.join(os.path.dirname(__file__), "table.csv"), "r") as f:
    table = csv.reader(f, delimiter=',', quotechar='"')
    header = next(table)
    for row in table:
        example = row[0]
        lang = row[1]
        cols = ["VerbForm", "Tense", "Aspect", "Mood", "Voice"]
        analysis = {col : sorted(row[2+j].split("+")) for j, col in enumerate(cols) if not row[2+j].startswith("[")}
        verbs = []
        row = ["#main#"] + row[7:]
        for i in range(0, len(row), 6):
            if row[i] == "":
                break
            verb = {"lemma" : row[i], "feats" : {}}
            for j, col in enumerate(cols):
                if not row[i+1+j].startswith("["):
                    verb["feats"][col] = row[i+1+j]
            verbs.append(verb)
        compound_verb = (verbs, analysis)
        try:
            LANG_TABLE[lang].append(compound_verb)
        except KeyError:
            LANG_TABLE[lang] = [compound_verb]