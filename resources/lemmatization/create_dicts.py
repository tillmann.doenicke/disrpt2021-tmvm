import json
import os
from conllu import parse_incr

data = os.path.join(os.path.dirname(__file__), "..", "tagger_parser_ud", "assets")
for corpus in os.listdir(data):
    path = os.path.join(data, corpus)
    if os.path.isdir(path):
        print(path)
        lemma_dict = {}
        lemma_dict_2 = {}
        test_data = []
        for file in os.listdir(path):
            if file.endswith("-train.conllu") or file.endswith("-dev.conllu"):
                lang = file.split("_")[0]
                with open(os.path.join(path, file), "r") as f:
                    for sentence in parse_incr(f):
                        for token in sentence:
                            form = token["form"]
                            upos = token["upos"]
                            lemma = token["lemma"]
                            try:
                                lemma_dict[form]
                            except KeyError:
                                lemma_dict[form] = {}
                            try:
                                lemma_dict[form][upos]
                            except KeyError:
                                lemma_dict[form][upos] = {}
                            try:
                                lemma_dict[form][upos][lemma] += 1
                            except KeyError:
                                lemma_dict[form][upos][lemma] = 1
                            try:
                                lemma_dict_2[form]
                            except KeyError:
                                lemma_dict_2[form] = {}
                            try:
                                lemma_dict_2[form][lemma] += 1
                            except KeyError:
                                lemma_dict_2[form][lemma] = 1
            elif file.endswith("-test.conllu"):
                with open(os.path.join(path, file), "r") as f:
                    for sentence in parse_incr(f):
                        for token in sentence:
                            form = token["form"]
                            upos = token["upos"]
                            lemma = token["lemma"]
                            test_data.append((form, upos, lemma))
        lemma_dict = {form : {upos : sorted(lemma_dict[form][upos].keys(), key=lambda lemma: lemma_dict[form][upos][lemma])[-1] for upos in lemma_dict[form]} for form in lemma_dict}
        lemma_dict_2 = {form : sorted(lemma_dict_2[form].keys(), key=lambda lemma: lemma_dict_2[form][lemma])[-1] for form in lemma_dict_2}
        correct = 0
        for form, upos, lemma in test_data:
            try:
                if lemma == lemma_dict[form][upos]:
                    correct += 1
            except KeyError:
                try:
                    if lemma == lemma_dict_2[form]:
                        correct += 1
                except KeyError:
                    if lemma == form:
                        correct += 1
        print(1.0*correct/len(test_data))
        outpath = os.path.join(os.path.dirname(__file__), lang + ".json")
        with open(outpath, "w") as f:
            json.dump((lemma_dict, lemma_dict_2), f, sort_keys=True, indent=2)