import os
import sys
import tqdm

from classifier import *
from file_reader import *
from utils.seg_eval import get_scores


if __name__ == "__main__":

    args = sys.argv
    if len(args) > 1:
        underscores_processed = bool(int(args[1]))
        if underscores_processed:
            corpora_with_underscores = []
        else:
            # corpora that don't have tokens (will be excluded in the plain condition)
            corpora_with_underscores = ["eng.pdtb.pdtb", "eng.rst.gum", "eng.rst.rstdt", "tur.pdtb.tdb", "zho.pdtb.cdtb"]
    else:
        raise Exception("You must specify whether the datasets with underscores are reconstructed (1) or not (0).")

    path_to_data = "data"

    for suffix in [".conllu", ".tok.spacy"]:
        results = {"p" : {},  "r" : {}, "f" : {}}
        for grammatical_features in [False, True]:
            for k in results:
                results[k][str(grammatical_features)] = {}

            vectorized_train_corpora = {}
            vectorized_dev_corpora = {}
            for corpus in tqdm.tqdm(sorted(os.listdir(path_to_data))):
                path_to_corpus = os.path.join(path_to_data, corpus)
                if os.path.isdir(path_to_corpus):
                    file = corpus + "_train" + suffix
                    if os.path.exists(os.path.join(path_to_corpus, file)):
                        documents_languages = read_data(file)
                        vectorized_corpus = prepare_data(documents_languages, grammatical_features)
                        vectorized_train_corpora[corpus] = vectorized_corpus
                    file = corpus + "_dev" + suffix
                    if os.path.exists(os.path.join(path_to_corpus, file)):
                        documents_languages = read_data(file)
                        vectorized_corpus = prepare_data(documents_languages, grammatical_features)
                        vectorized_dev_corpora[corpus] = vectorized_corpus

            for test_corpus in [str(key) for key in vectorized_train_corpora]:
                
                for train_corpus in [str(key) for key in vectorized_train_corpora] + ["ALL", "CV"]:
                    
                    if train_corpus in ["ALL", "CV"]:
                        if train_corpus == "CV" and len(vectorized_train_corpora) == 1:
                            continue
                        train_data = ([], [], [], [])
                        dev_data = ([], [], [], [])
                        for train_corpus_ in [str(key) for key in vectorized_train_corpora]:
                            if not (train_corpus == "CV" and test_corpus == train_corpus_):
                                train_data_ = vectorized_train_corpora[train_corpus_]
                                train_data = tuple(train_data[i]+train_data_[i] for i in range(len(train_data)))
                                dev_data_ = vectorized_dev_corpora[train_corpus_]
                                dev_data = tuple(dev_data[i]+dev_data_[i] for i in range(len(dev_data)))
                        path_to_model = os.path.join("models", str(grammatical_features) + "_" + train_corpus + ("_" + test_corpus if train_corpus == "CV" else "") + "_train" + suffix + ".pickle")
                        dev_data = None # CV and ALL yield the same results anyway

                    else:
                        train_data = vectorized_train_corpora[train_corpus]
                        dev_data = vectorized_dev_corpora[train_corpus]
                        path_to_model = os.path.join("models", str(grammatical_features) + "_" + train_corpus + "_train" + suffix + ".pickle")
                    
                    # training
                    if not os.path.exists(path_to_model):
                        train(path_to_model, train_data, dev_data, save_trees=True)
                    
                    # prediction
                    path_to_gold_file = os.path.join(path_to_data, test_corpus, test_corpus + "_test" + suffix)
                    path_to_pred_file = predict(path_to_model, path_to_gold_file, grammatical_features)
                    
                    # evaluation
                    scores = get_scores(path_to_gold_file, path_to_pred_file)
                    p, r, f = scores["prec"], scores["rec"], scores["f_score"]

                    try:
                        results["p"][str(grammatical_features)][train_corpus][test_corpus] = p
                        results["r"][str(grammatical_features)][train_corpus][test_corpus] = r
                        results["f"][str(grammatical_features)][train_corpus][test_corpus] = f
                    except KeyError:
                        results["p"][str(grammatical_features)][train_corpus] = {test_corpus : p}
                        results["r"][str(grammatical_features)][train_corpus] = {test_corpus : r}
                        results["f"][str(grammatical_features)][train_corpus] = {test_corpus : f}
        
        print(suffix, "f1-score")
        for train_corpus in sorted([str(key) for key in results["f"]["False"]]):
            row = [train_corpus]
            for test_corpus in sorted([str(key) for key in results["f"]["False"][train_corpus]]):
                row.append(str(round(100.0*results["f"]["False"][train_corpus][test_corpus])))
                row.append(str(round(100.0*results["f"]["True"][train_corpus][test_corpus])))
            print(" & ".join(row) + r"\\")
        print()

        print(suffix, "recall/precision/f1-score")
        for test_corpus in sorted([str(key) for key in results["f"]["True"]["ALL"]]):
            row = [test_corpus]
            grammatical_features = False
            if results["f"][str(True)][test_corpus][test_corpus] > results["f"][str(False)][test_corpus][test_corpus]:
                grammatical_features = True
            for k in ["r", "p", "f"]:
                row.append(str(round(100.0*results[k][str(grammatical_features)][test_corpus][test_corpus])))
            print(" & ".join(row) + r"\\")
        print()