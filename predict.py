import os
import sys

from classifier import predict

if __name__ == "__main__":
    args = sys.argv
    if len(args) > 2:
        path_to_model = args[1]
        path_to_test_file = args[2]
        path_to_pred_file = None
        if len(args) > 3:
            path_to_pred_file = args[3]

        grammatical_features = True
        predict(path_to_model, path_to_test_file, grammatical_features, path_to_pred_file)
    
    else:
        raise Exception("You must specify a model and a file.")